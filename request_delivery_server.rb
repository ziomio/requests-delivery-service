require 'eventmachine'
require 'em-http-request'
require 'evma_httpserver'
require 'json'
require_relative 'request_counter'

class RequestServer < EM::Connection
	include EM::HttpServer
	include EM::Deferrable

	def initialize(queue)
		puts RequestCounter.total_cnt
		@queue = queue

		@delay_bad_request = Proc.new do |http|
			puts "ERROR: #{http.error}, STATUS: #{http.response_header.status}, URL: #{http.req.uri}"
			EM.add_timer(5) do
				send_next_bad_request http.req.uri
     		end
		end
	end

	def process_http_request
		case @http_request_uri
		when "/send"
			if !get_url
				send_response 400, 'You must provide "url" parameter with http address'
				return
			end

			RequestCounter.increment_total_cnt
      		send_response "200", 'OK'
      		deliver_request_for get_url

		when "/stats"
			send_page "stats.html"

		when "/get_data"
			send_json_data
		else
			send_response 404, 'Not Found'
		end
    end

 private
 	def send_response(status = 200, body = 'OK', type = 'text/plain')
 		response = EM::DelegatedHttpResponse.new(self)
      	response.status = status
      	response.content_type type
      	response.content = body
      	response.send_response
 	end

	def send_page(filename)
		send_response 200, File.open(filename, "r") { |f| f.read }, 'text/html'
	end

	def send_json_data
		content = [Time.now.to_i * 1000,
				   RequestCounter.total_cnt,
				   RequestCounter.success_cnt,
				   calc_failed_requests_num].to_json
		send_response 200, content, 'text/json'
	end

	def calc_failed_requests_num
		RequestCounter.total_cnt - RequestCounter.success_cnt - @queue.size
	end

	def deliver_request_for(url)
		@queue.push url
		send_next_good_request
	end

	def analyze_http_response(http, request)
		puts "STATUS: #{http.response_header.status}, URL: #{http.req.uri}"
		if [200].include? http.response_header.status
			RequestCounter.increment_success_cnt
		elsif [301, 302].include? http.response_header.status
			puts "Redirection: #{http.response_header.location}"
			@queue.push http.response_header.location
		else
			request.fail http
		end
	end

   	def send_next_good_request
		@queue.pop do |url|
			request = EM::HttpRequest.new(url).get
			request.errback(&@delay_bad_request)

			request.callback do |http|
				analyze_http_response http, request
  				send_next_good_request unless @queue.empty?
			end
		end
	end

  	def send_next_bad_request(url)
  		request = EM::HttpRequest.new(url).get
  		request.errback(&@delay_bad_request)

		request.callback do |http|
			analyze_http_response http, request
		end
  	end

    def get_url
    	if @http_query_string.nil?
    		nil
    	else
    		result = @http_query_string.scan(/^url=(http:\/\/.+\..+)/)
    		result.flatten.first unless result.flatten.empty?
    	end
    end
end

EM.run do
	EM.start_server '127.0.0.1', 9000, RequestServer, EM::Queue.new
	puts "Started server at 127.0.0.1:9000"
end