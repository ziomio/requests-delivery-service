class RequestCounter
	def self.increment_total_cnt
        Mutex.new.synchronize do
		  @@total_cnt ||= 0
    	  @@total_cnt += 1
        end
    end

    def self.increment_success_cnt
        Mutex.new.synchronize do
    	  @@success_cnt ||= 0
    	  @@success_cnt += 1
        end
    end

    def self.success_cnt
    	@@success_cnt ||= 0
    	@@success_cnt
    end

    def self.total_cnt
    	@@total_cnt ||= 0
    	@@total_cnt
    end
end