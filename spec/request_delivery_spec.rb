require 'httparty'

describe "Guaranteed Request Delivery" do
	describe "valid uri" do
		describe "/send" do
			describe "valid address for url parameter" do
				it "should respond with code 200 and OK body" do
					response = HTTParty.get('http://127.0.0.1:9000/send?url=http://ya.ru')
					response.code.should eq 200
					response.body.should eq 'OK'
				end
			end
		end
		describe "/stats" do
			it "should return an html statistic page" do
				response = HTTParty.get('http://127.0.0.1:9000/stats')
				(response.body.include? 'Requests delivery chart').should eq true
			end
		end

		describe "/get_data" do
			it "should return an json string" do
				response = HTTParty.get('http://127.0.0.1:9000/get_data')
				puts response.body
				(response.body =~ /\[\d+\,\d+\,\d+\,\d+\]/).should_not eq nil
			end
		end
	end

	describe "invalid uri" do
		describe "url parameter" do
			describe "invalid address" do
				it "should respond with code 400" do
					response = HTTParty.get('http://127.0.0.1:9000/send?url=ya.')
					response.code.should eq 400
				end
			end

			describe "missing 'http://' specifier" do
				it "should respond with code 400" do
					response = HTTParty.get('http://127.0.0.1:9000/send?url=ya.ru')
					response.code.should eq 400
				end
			end
		end

		describe "action does not exist" do
			it "should respond with code 404" do
				response = HTTParty.get('http://127.0.0.1:9000/unknown_action?url=http://ya.ru')
				response.code.should eq 404
			end
		end
	end
end